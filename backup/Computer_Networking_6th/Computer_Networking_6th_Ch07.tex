\section{Multimedia Networking}
\begin{itemize}

\item
We define a multimedia network application as any network application that employs audio or video.

\item
Perhaps the most salient characteristic of video is its \textbf{high bit rate}.

\item
Another important characteristic of video is that it can be compressed, thereby trading off video quality with bit rate. There are two types of redundancy in video, both of which can be exploited by \textbf{video compression}. \textit{Spatial redundancy} is the redundancy within a given image. \textit{Temporal redundancy} reflects repetition from image to subsequent image.

\item
The analog audio signal is sampled at some fixed rate. Each of the samples is then rounded to one of a finite number of values. This operation is referred to as \textbf{quantization}. Each of the quantization values is represented by a fixed number of bits.\\
By increasing the sampling rate and the number of quantization values, the decoded signal can better approximate the original analog signal. Thus (as with video), there is a trade-off between the quality of the decoded signal and the bit-rate and storage requirements of the digital signal.\\
The basic encoding technique that we just described is called \textbf{pulse code modulation (PCM)}.

\item
A popular compression technique for near CD-quality stereo music is \textbf{MPEG 1 layer 3}, more commonly known as \textbf{MP3}. A related standard is \textbf{Advanced Audio Coding (AAC)}, which has been popularized by Apple.

\item
We classify multimedia applications into three broad categories: (1) streaming stored audio/video, (2) conversational voice/video-over-IP, and (3)streaming live audio/video.

\item
Streaming stored video has three key distinguishing features:(1) Streaming; (2) Interactivity; and (3) Continuous playout.

\item
Two axes---timing considerations and tolerance of data loss---are particularly important for conversational voice and video applications. Timing considerations are important because audio and video conversational applications are highly \textbf{delay-sensitive}. On the other hand, conversational multimedia applications are \textbf{loss-tolerant}---occasional loss only causes occasional glitches in audio/video playback, and these losses can often be partially or fully concealed.

\item
Streaming video systems can be classified into three categories: \textbf{UDP streaming}, \textbf{HTTP streaming,} and \textbf{adaptive HTTP streaming}.

\item
A common characteristic of all three forms of video streaming is the extensive use of client-side application buffering to mitigate the effects of varying end-to-end delays and varying amounts of available bandwidth between server and client.

\item
Before passing the video chunks to UDP, the server will encapsulate the video chunks within transport packets specially designed for transporting audio and video, using the Real-Time Transport Protocol (RTP) or a similar (possibly proprietary) scheme.

\item
Another distinguishing property of UDP streaming is that in addition to the server-to-client video stream, the clinet and the server also maintain, in parallel, a separate control connection over which the client sends commands regarding session state changes (such as pause, resume, reposition, and so on). This control connection is in many ways analogous to the FTP control connection. The Real-Time Streaming Protocol (RSTP) is a popular open protocol for such a control connection.

\item
However, for streaming \textit{stored} video, the client can attempt to download the video at a rate \textit{higher} than the consumption rate, thereby \textbf{prefetching} video frames that are to be consumed in the future.

\item
On the client side, the client application (media player) reads bytes from the TCP receive buffer (through its client socket) and places the bytes into the client application buffer. At the same time, the client application periodically grabs video frames from the client application buffer, decompresses the frames, and displays them on the user's screen.

\item
When the available rate in the network is less than the video rate, playout will alternate between periods of continuous playout and periods of freezing.

\item
When the available rate in the network is more than the video rate, after the initial buffering delay, the user will enjoy continuous playout until the video ends.

\item
HTTP streaming systems often make use of the \textbf{HTTP byte-range header} in the HTTP GET request message, which specifies the specific range of bytes the client currently wants to retrieve from the desired video.

\item
All clients receive the same encoding of the video, despite the large variations in the amount of bandwidth available to a client, both across different clients and also over time for the same client. This has led to the development of a new type of HTTP-based streaming, often referred to as \textbf{Dynamic Adaptive Streaming over HTTP (DASH)}. In DASH, the video is encoded into several different versions, with each version having a different bit rate and, correspondingly, a different quality level.

\item
With DASH, each video version is stored in the HTTP server, each with a different URL. The HTTP server also has a \textbf{manifest file}, which provides a URL for each version along with its bit rate.

\item
In order to meet the challenge of distributing massive amounts of video data to users distributed around the world, almost all major video-streaming companies make use of \textbf{Content Distribution Networks (CDNs)}. A CDN manages servers in multiple geographically distributed locations, stores copies of the videos (and other types of Web content, including documents, images, and audio) in its servers, and attempts to direct each user request to a CDN location that will provide the best user experience.

\item
CDNs typically adopt one of two different server placement philosophies: (1) Enter Deep; (2) Bring Home.

\item
At the core of any CDN deployment is a \textbf{cluster selection strategy}, that is, a mechanism for dynamically directing clients to a server cluster or a data center within the CDN.

\item
One simple strategy is to assign the client to the cluster that is \textbf{geographically closest}.

\item
In order to determine the best cluster for a client based on the \textit{current} traffic conditions, CDNs can instead perform periodic \textbf{real-time measurements} of delay and loss performance between their clusters and clients.

\item
An alternative to sending extraneous traffic for measuring path properties is to use the characteristics of recent and ongoing traffic between the clients and CDN servers.

\item
A very different approach to matching clients with CDN servers is to use \textbf{IP anycast}. The idea behind IP anycast is to have the routers in the Internet route the client's packets to the ``closest'' cluster, as determined by BGP.

\item
Real-time conversational voice over the Internet is often referred to as \textbf{Internet telephony}, since, from the user's perspective, it is similar to the traditional circuit-switched telephone service. It is also commonly called \textbf{Voice-over-IP (VoIP)}.

\item
VoIP applications often use some type of loss anticipation scheme. Two types of loss anticipation schemes are \textbf{forward error correction (FEC)} and \textbf{interleaving}.

\item
The basic idea of FEC is to add redundant information to the original packet stream. For the cost of marginally increasing the transmission rate, the redundant information can be used to reconstruct approximations or exact versions of some of the lost packet. We now outline two simple FEC mechanisms. The first mechanism sends a redundant encoded chunk after every \textit{n} chunks. The redundant chunk is obtained by exclusive OR-ing the \textit{n} original chunks. The second FEC mechanism is to send a lower-resolution audio stream as the redundant information.

\item
As an alternative to redundant transmission, a VoIP application can send interleaved audio. The sender resequences units of audio data before transmission, so that originally adjacent units are separated by a certain distance in the transmitted stream. Interleaving can mitigate the effect of packet losses.

\item
If \textit{both} Skype callers have NATs, then there is a problem---neither can accept a call initiated by the other, making a call seemingly impossible. The clever use of super peers and relays nicely solves this problem.

\item
Since most multimedia networking applications can make use of sequence numbers and timestamps, it is convenient to have a standardized packet structure that includes fields for audio/video data, sequence number, and timestamp, as well as other potentially useful fields. RTP is such a standard.

\item
The Session Initiation Protocol (SIP) is an open and lightweight protocol that does the following: (1) It provides mechanisms for establishing calls between a caller and a callee over an IP network; (2) It provides mechanisms for the caller to determine the current IP address of the callers; and (3) It provides mechanisms for call management.

\item
The question of how much capacity to provide at network links in a given topology to achieve a given level of performance is often known as \textbf{bandwidth provisioning}. The even more complicated problem of how to design a network topology (where to place routers, how to interconnect routers with links, and what capacity to assign to links) to achieve a given level of end-to-end performance is a network design problem often referred to as \textbf{network dimensioning}.

\item
\textbf{Insight 1: Packet marking} allows a router to distinguish among packets belonging to different classes of traffic.\\
\textbf{Insight 2:} It is desirable to provide a degree of \textbf{traffic isolation} among classes so that one class is not adversely affected by another class of traffic that misbehaves.\\
\textbf{Insight 3:} While providing isolation among classes or flows,  it is desirable to use resources (for example, link bandwidth and buffers) as efficiently as possible.\\
\textbf{Insight 4:} If sufficient resources will not always be available, and QoS is to be \textit{guaranteed}, a call admission process is needed in which flows declare their QoS requirements and are then either admitted to the network (at the required QoS) or blocked from the network (if the required QoS cannot be provided by the network).

\item
The FIFO (also known as first-come-first-served, or FCFS) scheduling discipline selects packets for link transmission in the same order in which they arrived at the output link queue.\\
When choosing a packet to transmit, the priority queuing discipline will transmit a packet from the highest priority class that has a nonempty queue (that is, has packets waiting for transmission). The choice among packets \textit{in the same priority} class is typically done in a FIFO manner.\\
Under the \textbf{round robin queuing discipline}, packets are sorted into classes as with priority queuing. However, rather than there being a strict priority of service among classes, a round robin scheduler alternates service among the classes.\\
A generalized abstraction of round robin queuing that has found considerable use in QoS architectures is the so-called \textbf{weighted fair queuing} (WFQ) discipline.

\item
One of our earlier insights was that policing, the regulation of the rate at which a class or flow is allowed to inject packets into the network, is an important QoS mechanism. But what aspects of a flow's packet rate should be policed? We can identify three important policing criteria, each differing from the other according to the time scale over which the packet flow is policed: (1) Average rate; (2) Peak rate; and (3) Burst size.\\
The leaky bucket mechanism is an abstraction that can be used to characterize these policing limits.

\item
Diffserv provides service differentiation---that is, the ability to handle different classes of traffic in different ways within the Internet in a scalable manner.\\
The Diffserv architecture consists of two sets of functional elements:\\
\hspace*{1em}(1) Edge functions: packet classification and traffic conditioning.\\
\hspace*{1em}(2) Core function: forwarding.

\item
In some cases, an end user may have have agreed to limit its packet-sending rate to conform to a declared \textbf{traffic profile}. The role of the \textbf{metering function} is to compare the incoming packet flow with the negotiated traffic profile and to determine whether a packet is within the negotiated traffic profile.\\
The second key component of the Diffserv architecture involves the per-hop behavior (PHB) performed by Diffserv-capable routers. PHB is rather cryptically, but carefully, defined as ``a description of the externally observable forwarding behavior of a Diffserv node applied to a particular Diffserv behavior aggregate''.

\item
Two PHBs have been defined: an expedited forwarding (EF) PHB and an assured forwarding (AF) PHB. The \textbf{expedited forwarding} PHB specifies that the departure rate of a class of traffic from a router must equal or exceed a configured rate. The \textbf{assured forwarding} PHB divides traffic into four classes, where each AF class is guaranteed to be provided with some minimum amount of bandwidth and buffering.

\item
The need for several new network mechanisms and protocols if a call (an end-to-end flow) is to be guaranteed a given quality of service once it begins: (1) Resource reservation; (2) Call admission; and (3) Call setup signaling.

\end{itemize}