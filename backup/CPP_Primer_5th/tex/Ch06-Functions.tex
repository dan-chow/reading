\section{Functions}
\begin{itemize}

\item
We execute a function through the \textbf{call operator}, which is a pair of parentheses.\\
A function call does two things: It initializes the function's parameters from the corresponding arguments, and it transfers control to that function.\\
Like a function call, the \texttt{return} statement does two things: It returns the value (if any) in the \texttt{return}, and it transfers control out of the \textit{called} function back to the \textit{calling} function.

\item
Objects that exist only while a block is executing are known as \textbf{automatic objects}.

\item
It can be useful to have a local variable whose lifetime continues across calls to the function. We obtain such objects by defining a local variable as \texttt{static}. Each \textbf{local \texttt{static} object} is initialized before the \textit{first} time execution passes through the object's definition.

\item
To allow programs to be written in logical parts, C\texttt{++} supports what is commonly known as \textbf{\textit{separate compilation}}.

\item
When a parameter is a reference, we say that its corresponding argument is \texttt{"}\textbf{passed by reference}\texttt{"} or that the function is \texttt{"}\textbf{called by reference}\texttt{"}. As with any other reference, a reference parameter is an alias for the object to which it is bound; that is, the parameter is an alias for its corresponding argument.\\
When the argument value is copied, the parameter and argument are independent objects. We say such arguments are \texttt{"}\textbf{passed by value}\texttt{"} or \texttt{"}\textbf{called by value}\texttt{"}.

\item
Programmers accustomed to programming in C often use pointer parameters to access objects outside a function. In C\texttt{++}, programmers generally use reference parameters instead.

\item
It can be inefficient to copy objects of large class types or large containers. Moreover, some class types (including the IO types) cannot be copied. Functions must use reference parameters to operate on objects of a type that cannot be copied.\\
Reference parameters that are not changed inside a function should be references to \texttt{const}.\\
Reference parameters let us effectively return multiple results.

\item
Arrays have two special properties that affect how we define and use functions that operate on arrays: We cannot copy an array, and when we use an array it is (usually) converted to a pointer. Because we cannot copy an array, we cannot pass an array by value. Because arrays are converted to pointers, when we pass an array to a function, we are actually passing a pointer to the array's first element.

\item
\hspace*{1em}\texttt{f(int \&arr[10]) // }\textit{error: declares arr as an array of references}\\
\hspace*{1em}\texttt{f(int (\&arr)[10]) // }\textit{ok: arr is a reference to an array of ten ints}\\
The parentheses around \texttt{\&arr} are necessary.\\
\hspace*{1em}\texttt{int *matrix[10]; // }\textit{array of ten pointers}\\
\hspace*{1em}\texttt{int (*matrix)[10]; // }\textit{pointer to an array of ten ints}\\
Again, the parentheses around \texttt{*matrix} are necessary.

\item
When you use the arguments in \texttt{argv}, remember that the optional arguments begin in \texttt{argv[1]}; \texttt{argv[0]} contains the program's name, not user input.

\item
The new standard provides two primary ways to write a function that takes a varying number of arguments: If all the arguments have the same type, we can pass a library type named \texttt{initializer\_list}. If the argument types vary, we can write a special kind of function, known as a variadic template.\\
We can write a function that takes an unknown number of arguments of a single type by using an\\
\textbf{\texttt{initializer\_list}} parameter.

\begin{figure}[ht]
\includegraphics[scale=0.7]{table-6-1}
\centering
\caption{Operations on \texttt{initializer\_list}s}
\end{figure}

\item
\textbf{Never Return a Reference or Pointer to a Local Object}

\item
Under the new standard, functions can return a braced list of values.

\item
Under the new standard, another way to simplify the declaration of \texttt{func} is by using a \textbf{trailing return type}.\\
\hspace*{1em}\texttt{// }\textit{fcn takes an int argument and returns a pointer to an array of ten ints}\\
\hspace*{1em}\texttt{auto func(int i)->int(*)[10];}

\item
A parameter that has a top-level \texttt{const} is indistinguishable from one without a top-level \texttt{const}.\\
\hspace*{1em}\texttt{Record lookip(Phone);}\\
\hspace*{1em}\texttt{Record lookup(const Phone); // }\textit{redeclares Record lookup(Phone)}\\
On the other hand, we can overload based on whether the parameter is a reference (or pointer) to the \texttt{const} or non\texttt{const} version of a given type; such \texttt{const}s are low-level.\\
\hspace*{1em}\texttt{Record lookup(Account\&); // }\textit{function that takes a reference to Account}\\
\hspace*{1em}\texttt{Record lookup(const Account\&); // }\textit{new function that takes a const reference}

\item
\textbf{Function matching} (also known as \textbf{overload resolution}) is the process by which a particular function call is associated with a specific function from a set of overloaded functions.\\
In C\texttt{++}, name lookup happens before type checking.

\item
Some functions have parameters that are given a particular value in most, but not all, calls. In such cases, we can declare that common value as a \textbf{default argument} for the function.\\
Although it is normal practice to declare a function once inside a header, it is legal to redeclare a function multiple times. However, each parameter can have its default specified only once in a given scope. Thus, any subsequent declaration can add a default only for a parameter that has not previously had a default specified.\\
Names used as default arguments are resolved in the scope of the function declaration. The value that those names represent is evaluated at the time of the call.\\
\hspace*{1em}\texttt{// }\textit{the declarations of wd, def, and ht must appear outside a function}\\
\hspace*{1em}\texttt{sz wd = 80;}\\
\hspace*{1em}\texttt{char def = ' ';}\\
\hspace*{1em}\texttt{sz ht();}\\
\hspace*{1em}\texttt{string screen(sz = ht(), sz = wd, char = def);}\\
\hspace*{1em}\texttt{string window = screen(); // }\textit{calls screen(ht(), 80, ' ')}\\
\hspace*{1em}\texttt{void f2()}\\
\hspace*{1em}\texttt{$\{$}\\
\hspace*{3em}\texttt{def = '*';		// }\textit{changes the value of a default argument}\\
\hspace*{3em}\texttt{sz wd = 100;	// }\textit{hides the outer definition of wd but does not change the default}\\
\hspace*{3em}\texttt{window = screen(); // }\textit{calls screen(ht(), 80, '*')}\\
\hspace*{1em}\texttt{$\}$}

\item
A function specified as \textbf{\texttt{inline}} (usually) is expanded \texttt{"}in line\texttt{"} at each call.

\item
A \textbf{\texttt{constexpr} function} is a function that can be used in a constant expression.\\
A \texttt{constexpr} function is not required to return a constant expression.

\item
C\texttt{++} programmers sometimes use a technique similar to header guards to conditionally execute debugging code. The idea is that the program will contain debugging code that is executed only while the program is being developed. When the application is completed and ready to ship, the debugging code is turned off. This approach uses two preprocessor facilities: \texttt{assert} and \texttt{NDEBUG}.\\
\textbf{\texttt{assert}} is a \textbf{preprocessor macro}. A preprocessor macro is a preprocessor variable that acts somewhat like an inline function.\\
The behavior of \texttt{assert} depends on the status of a preprocessor variable named \texttt{NDEBUG}. If \texttt{NDEBUG} is defined, \texttt{assert} does nothing. By default, \texttt{NDEBUG} is \textit{not} defined, so, by default, \texttt{assert} performs a run-time check.

\item
In order to analyze a call, it is important to remember that the small integral types always promote to \texttt{int} or to a larger integral type.\\
\hspace*{1em}\texttt{void ff(int);}\\
\hspace*{1em}\texttt{void ff(short);}\\
\hspace*{1em}\texttt{ff('a'); // }\textit{char promotes to int; calls f(int)}\\
All the arithmetic conversions are treated as equivalent to each other.\\
\hspace*{1em}\texttt{void manip(long);}\\
\hspace*{1em}\texttt{void manip(float);}\\
\hspace*{1em}\texttt{manip(3.14); // }\textit{error: ambiguous call}

\item
A function pointer is just that---a pointer that denotes a function rather than an object. Like any other pointer, a function pointer points to a particular type. A function's type is determined by its return type and the types of its parameters.

\end{itemize}