\section{Variables and Basic Types}
\begin{itemize}

\item
C\texttt{++} defines a set of primitive types that include the \textbf{arithmetic types} and a special type named \texttt{void}.

\item
The type of a string literal is \textbf{\textit{array}} of constant \texttt{char}s. The compiler appends a null character (\texttt{'}\textbackslash\texttt{0'}) to every string literal. Thus, the actual size of a string literal is one more than its apparent size.

\item
Most generally, an object is a region of memory that can contain data and has a type.

\item
Initialization is not assignment. Initialization happens when a variable is given a value when it is created. Assignment obliterates an object's current value and replaces that value with a new one.

\item
The generalized use of curly braces for initialization was introduced as part of the new standard. This form of initialization previously had been allowed only in more restricted ways. This form of initialization is referred to as \textbf{list initialization}. Braced lists of initializers can now be used whenever we initialize an object and in some cases when we assign a new value to an object.\\
When used with variables of built-in type, this form of initialization has one important property: The compiler will not let us list initialize variables of built-in type if the initializer might lead to the loss of information:\\
\hspace*{1em}\texttt{long double ld = 3.1415926536;}\\
\hspace*{1em}\texttt{int a$\{$ld$\}$, b = $\{$ld$\}$;} // \textit{error: narrowing conversion required}\\\hspace*{1em}\texttt{int c(ld), d = ld;} // \textit{ok: but value will be truncated}

\item
Variables defined outside any function body are initialized to zero. With one exception, variables of built-in type defined inside a function are \textbf{uninitialized}. The value of an uninitialized variable of built-in type is undefined.

\item
A \textbf{declaration} makes a name known to the program. A file that wants to use a name defined elsewhere includes a declaration for that name. A \textbf{definition} creates the associated entity.

\item
To obtain a declaration that is not also a definition, we add the \texttt{extern} keyword and may not provide an explicit initializer:\\
\hspace*{1em}\texttt{extern int i; // }\textit{declares but does not define i}\\
\hspace*{1em}\texttt{int j; // }\textit{declares and defines j}

\item
A \textbf{scope} is a part of the program in which a name has a particular meaning.

\item
A \textbf{compound type} is a type that is defined in terms of another type.\\
A declaration is a \textbf{base type} followed by a list of \textbf{declarators}. Each declarator names a variable and gives the variable a type that is related to the base type.

\item
A \textbf{reference} defines an alternative name for an object. A reference type \texttt{"}refers to\texttt{"} another type. We define a reference type by writing a declarator of the form \texttt{\&d}, where \texttt{d} is the name being declared.\\
Ordinarily, when we initialize a variable, the value of the initializer is copied into the object we are creating. When we define reference, instead of copying the initializer's value, we \textbf{bind} the reference to its initializer. Once initialized, a reference remains bound to its initial object. There is no way to rebind a reference to refer to a different object. Because there is no way to rebind a reference, reference \textit{must} be initialized.\\
We can define multiple references in a single definition. Each identifier that is a reference must be preceded by the \texttt{\&} symbol.

\item
A \textbf{pointer} is a compound type that \texttt{"}points to\texttt{"} another type. Like reference, pointers are used for indirect access to other objects. Unlike a reference, a pointer is an object in its own right. Pointers can be assigned and copied; a single pointer can point to several different objects over its lifetime. Unlike a reference, a pointer need not be initialized at the time it is defined. Like other built-in types, pointers defined at block scope have undefined value if they are not initialized.\\
We define a pointer type by writing a declarator of the form \texttt{*d}, where \texttt{d} is the name being defined. The \texttt{*} must be repeated for each pointer variable.\\
A pointer holds the address of another object. We get the address of an object by using the address-of operator (the \textbf{\texttt{\&} operator}).
\\When a pointer points to an object, we can use the dereference operator (the \textbf{\texttt{*} operator}) to access that object.

\item
Preprocessors variables are managed by the preprocessor, and are not part of the \texttt{std} namespace.\\
Modern C\texttt{++} programs generally should avoid using \texttt{NULL} and use \texttt{nullptr} instead.

\item
If possible, define a pointer only after the object to which it should point has been defined. If there is no object to bind to a pointer, then initialize the pointer to \texttt{nullptr} or zero. That way, The program can detect that the pointer does not point to an object.

\item
Two pointers are equal if they hold the same address and unequal otherwise.

\item
Generally, we use a \texttt{void*} pointer to deal with memory as memory, rather than using the pointer to access the object stored in that memory.

\item
It is a common misconception to think that the type modifier (\texttt{*} or \texttt{\&}) applies to all the variables defined in a single statement.

\item
\hspace*{1em}\texttt{int i = 42;}\\
\hspace*{1em}\texttt{int *p; // }\textit{p is a pointer to int}\\
\hspace*{1em}\texttt{int *\&r = p; // }\textit{r is a reference to the pointer p}\\
The easiest way to understand the type of \texttt{r} is to read the definition right to left. The symbol closest to the name of the variable (in this case the \texttt{\&} in \texttt{\&r}) is the one that has the most immediate effect on the variable's type.

\item
We can make a variable unchangeable by defining the variable's type as \texttt{const}.

\item
To define a single instance of a \texttt{const} variable, we use the keyword \texttt{extern} on both its definition and declaration(s):\\
\hspace*{1em}\texttt{// }\textit{file\_1.cc defines and initializes a const that is accessible to other files}\\
\hspace*{1em}\texttt{extern const int bufSize = fcn();}\\
\hspace*{1em}\texttt{// }\textit{file\_1.h}\\
\hspace*{1em}\texttt{extern const int bufSize; // }\textit{same bufSize as defined in file\_1.cc}

\item
As with any other object, we can bind a reference to an object of a \texttt{const} type. To do so we use a \textbf{reference to \texttt{const}}, which is a reference that refers to a \texttt{const} type;

\item
There are two exceptions to the rule that the type of a reference must match the type of the object to which it refers. The first exception is that we can initialize a reference to \texttt{const} from any expression that can be converted to the type of the reference. In particular, we can bind a reference to \texttt{const} to a non\texttt{const} object, a literal, or a more general expression.


\item
\hspace*{1em}\texttt{double dval = 3.14;}\\
\hspace*{1em}\texttt{const int \&ri = dval;}\\
Here \texttt{ri} refers to an \texttt{int}. Operations on \texttt{ri} will be integer operations, but \texttt{dval} is a floating-point number, not an integer. To ensure that the object to which \texttt{ri} is bound is an \texttt{int}, the compiler transforms this code into something like:\\
\hspace*{1em}\texttt{const int temp = dval; // }\textit{create a temporary const int from the double}\\
\hspace*{1em}\texttt{const int \&ri = temp; // }\textit{bind ri to that temporary}\\
In this case, \texttt{ri} is bound to a \textbf{temporary} object. A temporary object is an unnamed object created by the compiler when it needs a place to store a result from evaluating an expression.

\item
Like a reference to \texttt{const}, a \textbf{pointer to \texttt{const}} may not be used to change the object to which the pointer points.

\item
There are two exceptions to the rule that the types of a pointer and the object to which it points must match. The first exception is that we can use a pointer to \texttt{const} to point to a non\texttt{const} object.\\
Like a reference to \texttt{const}, a point to \texttt{const} says nothing about whether the object to which the pointer points is \texttt{const}. Defining a pointer as a pointer to \texttt{const} affects only what we can do with the pointer. It is important to remember that there is no guarantee that an object pointed to by a pointer to \texttt{const} won't change.

\item
Unlike references, pointers are objects. Hence, as with any other type, we can have a pointer that is itself \texttt{const}. Like any other \texttt{const} object, a \textbf{\texttt{const} pointer} must be initialized, and once initialized, its value (i.e., the address that is holds) may not be changed. We indicate that the pointer is \texttt{const} by putting the \texttt{const} after the \texttt{*}.

\item
The fact that a pointer is itself \texttt{const} says nothing about whether we can use the pointer to change the underlying object.

\item
We use the term \textbf{top-level \texttt{const}} to indicate that the pointer itself is a \texttt{const}. When a pointer can point to a \texttt{const} object, we refer to that \texttt{const} as a \textbf{low-level \texttt{const}}.

\item
A \textbf{constant expression} is an expression whose value cannot change and that can be evaluated at compile time.

\item
Under the new standard, we can ask the compiler to verify that a variable is a constant expression by declaring the variable in a \textbf{constexpr} declaration.

\item
A \textbf{type alias} is a name that is a synonym for another type.\\
We can define a type alias in one of two ways. Traditionally, we use a \textbf{typedef}:\\
\hspace*{1em}\texttt{typedef double wages; // }\textit{wages is a synonym for double}\\
The new standard introduced a second way to define a type alias, via an \textbf{alias declaration}:\\
\hspace*{1em}\texttt{using SI = Sales\_item; // }\textit{SI is a synonym for Sales\_item}

\item
Under the new standard, we can let the compiler figure out the type for us by using the \textbf{\texttt{auto}} type specifier.\\
The type that the compiler infers for \texttt{auto} is not always exactly the same as the initializer's type. Instead, the compiler adjusts the type to conform to normal initialization rules.\\
First, when we use a reference as an initializer, the initializer is the corresponding object. The compiler uses that object's type for \texttt{auto}'s type deduction.\\
Second, \texttt{auto} ordinarily ignores top-level \texttt{const}s. As usual in initializations, low-level \texttt{const}s, such as when an initializer is a pointer to \texttt{const}, are kept.\\
When we ask for a reference to an \texttt{auto}-deduced type, top-level \texttt{const}s in the initializer are not ignored.

\item
Sometimes we want to define a variable with a type that the compiler deduces from an expression but do not want to use that expression to initialize the variable. For such cases, the new standard introduced a second type specifier, \textbf{decltype}, which returns the type of its operand. The compiler analyzes the expression to determine its type but does not evaluate the expression.

\item
As we've seen, when we dereference a pointer, we get the object to which the pointer points. Moreover, we can assign to that object. Thus, the type deduced by \texttt{decltype(*p)} is \texttt{int\&}, not plain \texttt{int}.

\item
When we apply \texttt{decltype} to a variable without any parentheses, we get the type of that variable. If we wrap the variable's name in one or more sets of parentheses, the compiler will evaluate the operand as an expression. A variable is an expression that can be the left-hand side of an assignment. As a result, \texttt{decltype} on such an expression yields a reference:\\
\hspace*{1em}\texttt{// }\textit{decltype of a parenthesized variable is always a reference}\\
\hspace*{1em}\texttt{decltype((i)) d; // }\textit{error: d is int\& and must be initialized}\\
\hspace*{1em}\texttt{decltype(i) e; // }\textit{ok: e is an (uninitialized) int}

\item
It is a common mistake among new programmers to forget the semicolon at the end of a class definition.

\item
Under the new standard, we can supply an \textbf{in-class initializer} for a data member. When we create objects, the in-class initializers will be used to initialize the data members.

\item
C\texttt{++} programs also use the preprocessor to define \textbf{header guards}. Header guards rely on preprocessor variables.\\
Headers should have guards, even if they aren't (yet) included by another header.

\end{itemize}